package com.baiyue.admin.entity;

import java.util.Date;

/**
 * 用户登录信息.
 * @date 2014/2/25
 */
public class UserLogin {
	private String userId;
	private int loginCount;
	private Date lastLogin;
	private int online;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public int getOnline() {
		return online;
	}

	public void setOnline(int online) {
		this.online = online;
	}
}
