package com.baiyue.admin.entity;

import java.util.Date;

/**
 * 用户信息.
 * @date   2014/2/25
 */
public class UserInfo {

	private String userId;
	private String username;
	private String password;
	private Date   regDate;
	private String realName;
	private String sex;
	private int    age;
	private Date   birthday;
	private String phone;
	private String email;
	private String address;
	private String askQuestion;
	private String askAnswer;
	
	private UserLogin userLogin;
	
	public UserLogin getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(UserLogin userLogin) {
		this.userLogin = userLogin;
	}

	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAskQuestion() {
		return askQuestion;
	}
	public void setAskQuestion(String askQuestion) {
		this.askQuestion = askQuestion;
	}
	public String getAskAnswer() {
		return askAnswer;
	}
	public void setAskAnswer(String askAnswer) {
		this.askAnswer = askAnswer;
	}
	
}
