package com.baiyue.admin.entity;


/**
 * 订单状态.
 * @date   2014年2月27日
 */
public final  class OrderStatus {
	/** 订单生成. */
	public static final String ORDER_STATUS_GENERATION = "generation";
	/** 已支付. */
	public static final String ORDER_STATUS_PAYOVER	   = "payover";
	/** 送货中. */
	public static final String ORDER_STATUS_DELIVERY   = "delivery";
	/** 订单完成. */
	public static final String ORDER_STATUS_DONE   	   = "done";
	
	private OrderStatus(){
		
	}
	
}
