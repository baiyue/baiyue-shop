package com.baiyue.admin.entity;

import java.util.Date;

/**
 * 购物车条项.
 * @date   2014年2月27日
 */
public class ShoppingCartItem {
	
	private String		cartItemId;
	private BookInfo	bookInfo;
	private int			count;
	private Date		date;
	
	public String getCartItemId() {
		return cartItemId;
	}
	public void setCartItemId(String cartItemId) {
		this.cartItemId = cartItemId;
	}
	public BookInfo getBookInfo() {
		return bookInfo;
	}
	public void setBookInfo(BookInfo bookInfo) {
		this.bookInfo = bookInfo;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
