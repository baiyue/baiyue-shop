package com.baiyue.admin.entity;

import java.util.Date;

/**
 * 用户评论信息.
 * @date 2014/2/25
 */
public class ReviewInfo {
	private String reviewId;
	private String bookId;
	private String userId;
	private String username;
	private String message;
	private Date   date;
	//是否审核
	private int checkTag;

	public String getReviewId() {
		return reviewId;
	}

	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCheckTag() {
		return checkTag;
	}

	public void setCheckTag(int checkTag) {
		this.checkTag = checkTag;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
