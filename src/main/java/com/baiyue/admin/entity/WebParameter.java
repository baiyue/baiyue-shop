package com.baiyue.admin.entity;

/**
 * 网站参数设置.
 * @date   2014/2/25
 */
public class WebParameter {
	private String version;
	private String webName;
	private String regClause;
	private String notice;
	private String postcode;
	private String telphone;
	private String copyright;
	private String weblogo;
	private String webSite;
	private String payment;
	private String shopstream;
	private String postMethod;
	private String postPrice;
	private String postDescription;
	private String workTime;
	private String service;
	private String law;
	private String comques;
	private String dealrule;

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}

	public String getRegClause() {
		return regClause;
	}

	public void setRegClause(String regClause) {
		this.regClause = regClause;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getWeblogo() {
		return weblogo;
	}

	public void setWeblogo(String weblogo) {
		this.weblogo = weblogo;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getShopstream() {
		return shopstream;
	}

	public void setShopstream(String shopstream) {
		this.shopstream = shopstream;
	}

	public String getPostMethod() {
		return postMethod;
	}

	public void setPostMethod(String postMethod) {
		this.postMethod = postMethod;
	}

	public String getPostPrice() {
		return postPrice;
	}

	public void setPostPrice(String postPrice) {
		this.postPrice = postPrice;
	}

	public String getPostDescription() {
		return postDescription;
	}

	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getLaw() {
		return law;
	}

	public void setLaw(String law) {
		this.law = law;
	}

	public String getComques() {
		return comques;
	}

	public void setComques(String comques) {
		this.comques = comques;
	}

	public String getDealrule() {
		return dealrule;
	}

	public void setDealrule(String dealrule) {
		this.dealrule = dealrule;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
