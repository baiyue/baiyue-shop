package com.baiyue.admin.entity;

import java.util.Date;
import java.util.Set;

/**
 * 图书信息.
 * @date   2014/2/25
 */
public class BookInfo {

	private String 	bookId;
	private String 	bookName;
	private String 	bookType;
	private String 	publisher;
	private Date 	publishDate;
	private int 	bookSize;
	private String 	edition;  	//版本
	private String 	author;
	private String 	packStyle; 	//封装类型
	private String 	translator; //译者
	private String 	bookCover; 	//封皮图片ַ
	private String 	bookIsbn;
	private float 	bookPrice;
	private String 	description;
	private String 	catalogue;  //目录
	private Date 	addTime;    //入库时间
	private float 	salePrice;  //销售价
	private float 	memberPrice;//会员价
	private int 	saleCount;  //售卖量
	private String 	bookStock;  //库存量

	private Set<?>  reviewInfos;
	
	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public int getBookSize() {
		return bookSize;
	}

	public void setBookSize(int bookSize) {
		this.bookSize = bookSize;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPackStyle() {
		return packStyle;
	}

	public void setPackStyle(String packStyle) {
		this.packStyle = packStyle;
	}

	public String getTranslator() {
		return translator;
	}

	public void setTranslator(String translator) {
		this.translator = translator;
	}

	public String getBookCover() {
		return bookCover;
	}

	public void setBookCover(String bookCover) {
		this.bookCover = bookCover;
	}

	public String getBookIsbn() {
		return bookIsbn;
	}

	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}

	public float getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(float bookPrice) {
		this.bookPrice = bookPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCatalogue() {
		return catalogue;
	}

	public void setCatalogue(String catalogue) {
		this.catalogue = catalogue;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}

	public float getMemberPrice() {
		return memberPrice;
	}

	public void setMemberPrice(float memberPrice) {
		this.memberPrice = memberPrice;
	}

	public int getSaleCount() {
		return saleCount;
	}

	public void setSaleCount(int saleCount) {
		this.saleCount = saleCount;
	}

	public String getBookStock() {
		return bookStock;
	}

	public void setBookStock(String bookStock) {
		this.bookStock = bookStock;
	}

	public Set<?> getReviewInfos() {
		return reviewInfos;
	}

	public void setReviewInfos(Set<?> reviewInfos) {
		this.reviewInfos = reviewInfos;
	}
}
