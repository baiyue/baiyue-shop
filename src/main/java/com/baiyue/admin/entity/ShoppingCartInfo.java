package com.baiyue.admin.entity;

import java.util.Set;

/**
 * 购物车信息.
 * @date 	2014/2/25
 */
public class ShoppingCartInfo {

	private String cartInfoId;
	private Set<ShoppingCartItem>  cartItems;
	
	
	
	public Set<ShoppingCartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(Set<ShoppingCartItem> cartItems) {
		this.cartItems = cartItems;
	}
	public String getCartInfoId() {
		return cartInfoId;
	}
	public void setCartInfoId(String cartInfoId) {
		this.cartInfoId = cartInfoId;
	}

}
