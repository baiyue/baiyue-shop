package com.baiyue.admin.entity;

public final class Operation {
	
	public final static String ADMIN_LIST   = "AdminList";
	public final static String ADMIN_DELETE = "AdminDelete";
	public final static String ADMIN_UPDATE = "AdminUpdate";
	public final static String ADMIN_INSERT = "AdminInsert";
	/**BookAdmin允许的操作.*/
	public final static String BOOK_INSERT = "BookInsert";
	public final static String BOOK_DELETE = "BookDelete";
	public final static String BOOK_MODIFY = "BookModify";
	/**OrderAdmin允许的操作.*/
	public final static String ORDER_PROCESS= "OrderProcess";
	public final static String ORDER_DELETE = "OrderDelete";
	/**Staff允许的操作.*/
	public final static String REPORT_TABLE = "ReportTable";
}
