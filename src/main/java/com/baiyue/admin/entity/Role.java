package com.baiyue.admin.entity;

/** 
 * 角色.
 * @date   2014年2月27日
 */
public final class Role {
	/**根管理员,只有一个. */
	public static final String ROLE_ROOT 		= "Root";
	/**图书管理员.*/
	public static final String ROLE_BOOK_ADMIN 	= "BookAdmin";
	/**订单管理员.*/
	public static final String ROLE_ORDER_ADMIN = "OrderAdmin";
	/**报表员工.*/
	public static final String ROLE_STAFF = "Staff";
	/**游客.*/
	public static final String ROLE_GUEST = "Guest";
}
