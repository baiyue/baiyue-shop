package com.baiyue.admin.entity;

import java.util.Date;
import java.util.Set;

/**
 * 订单信息.
 * @date 2014/2/25
 */
public class OrderInfo {
	private String 		orderInfoId;
	private UserInfo 	userInfo;
	private UserAddress addressInfo;
	private Date 		orderDate;
	private int 		bookCount;
	private String	 	message;
	private float 		totalPrice;
	private String 		comment;
	private String 		status;
	private Set<?>      orderItems;
	
	public String getOrderInfoId() {
		return orderInfoId;
	}

	public void setOrderInfoId(String orderInfoId) {
		this.orderInfoId = orderInfoId;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public UserAddress getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(UserAddress addressInfo) {
		this.addressInfo = addressInfo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public int getBookCount() {
		return bookCount;
	}

	public void setBookCount(int bookCount) {
		this.bookCount = bookCount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<?> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Set<?> orderItems) {
		this.orderItems = orderItems;
	}

}
