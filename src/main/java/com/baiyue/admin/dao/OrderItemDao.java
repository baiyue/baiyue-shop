package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.OrderItem;

public interface OrderItemDao extends BaseDaoOperation<OrderItem> {

}
