package com.baiyue.admin.dao;


import java.util.List;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.AdminInfo;

/**
 * 
 * @date  2014年3月1日
 */
public interface AdminInfoDao extends BaseDaoOperation<AdminInfo> {
	
	AdminInfo getAdminInfoByName(String name);
	List<AdminInfo> getAllAdminInfo();
	boolean deleteAdminById(String adminId);
}
