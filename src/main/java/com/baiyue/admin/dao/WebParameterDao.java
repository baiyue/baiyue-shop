package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.WebParameter;

public interface WebParameterDao extends BaseDaoOperation<WebParameter> {

}
