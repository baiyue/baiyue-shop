package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.OrderInfo;

public interface OrderInfoDao extends BaseDaoOperation<OrderInfo> {

}
