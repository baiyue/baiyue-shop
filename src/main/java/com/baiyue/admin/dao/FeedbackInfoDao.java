package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.FeedbackInfo;

/**
 * 
 * @date  2014年3月1日
 */
public interface FeedbackInfoDao extends BaseDaoOperation<FeedbackInfo> {

}
