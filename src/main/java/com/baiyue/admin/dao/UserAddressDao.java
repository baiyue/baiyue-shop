package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.UserAddress;

public interface UserAddressDao extends BaseDaoOperation<UserAddress> {

}
