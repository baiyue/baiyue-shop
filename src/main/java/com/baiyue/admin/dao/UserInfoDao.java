package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.UserInfo;

public interface UserInfoDao extends BaseDaoOperation<UserInfo> {

}
