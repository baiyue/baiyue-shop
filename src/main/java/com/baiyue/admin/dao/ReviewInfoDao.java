package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.ReviewInfo;

public interface ReviewInfoDao extends BaseDaoOperation<ReviewInfo> {

}
