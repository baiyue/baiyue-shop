package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.UserLogin;

public interface UserLoginDao extends BaseDaoOperation<UserLogin> {

}
