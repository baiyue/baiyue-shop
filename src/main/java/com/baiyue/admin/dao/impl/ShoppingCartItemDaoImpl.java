package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.ShoppingCartItemDao;
import com.baiyue.admin.entity.ShoppingCartItem;
/**
 * 购物车具体条目信息数据库处理.
 * @date  2014年3月1日
 */
@Repository
public class ShoppingCartItemDaoImpl extends BaseDao<ShoppingCartItem> implements
		ShoppingCartItemDao {

	public ShoppingCartItemDaoImpl() {
		super(ShoppingCartItem.class);
		
	}


}
