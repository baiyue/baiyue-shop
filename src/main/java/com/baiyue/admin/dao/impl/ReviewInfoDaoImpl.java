package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.ReviewInfoDao;
import com.baiyue.admin.entity.ReviewInfo;

/**
 * 图书评论信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class ReviewInfoDaoImpl extends BaseDao<ReviewInfo> implements ReviewInfoDao {
	public ReviewInfoDaoImpl() {
		super(ReviewInfo.class);
		
	}

}
