package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.OrderItemDao;
import com.baiyue.admin.entity.OrderItem;
/**
 * 订单详细信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class OrderItemDaoImpl extends BaseDao<OrderItem> implements OrderItemDao {

	public OrderItemDaoImpl() {
		super(OrderItem.class);
	}

}
