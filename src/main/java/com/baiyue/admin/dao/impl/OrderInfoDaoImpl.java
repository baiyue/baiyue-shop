package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.OrderInfoDao;
import com.baiyue.admin.entity.OrderInfo;

/**
 * 订单信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class OrderInfoDaoImpl extends BaseDao<OrderInfo> implements OrderInfoDao {

	public OrderInfoDaoImpl() {
		super(OrderInfo.class);
	}

}
