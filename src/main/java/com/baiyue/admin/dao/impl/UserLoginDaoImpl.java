package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.UserLoginDao;
import com.baiyue.admin.entity.UserLogin;
/**
 * 用户登录信息数据库处理.
 * @date  2014年3月1日
 */
@Repository
public class UserLoginDaoImpl extends BaseDao<UserLogin> implements UserLoginDao {

	public UserLoginDaoImpl() {
		super(UserLogin.class);
	}

}
