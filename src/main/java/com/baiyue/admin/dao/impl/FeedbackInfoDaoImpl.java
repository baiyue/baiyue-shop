package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.FeedbackInfoDao;
import com.baiyue.admin.entity.FeedbackInfo;
/**
 * 顾客反馈信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class FeedbackInfoDaoImpl extends BaseDao<FeedbackInfo> implements FeedbackInfoDao {
	
	public FeedbackInfoDaoImpl() {
		super(FeedbackInfo.class);
	}

}
