package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.UserAddressDao;
import com.baiyue.admin.entity.UserAddress;
/**
 * 用户收货地址信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class UserAddressDaoImpl extends BaseDao<UserAddress> implements UserAddressDao {

	public UserAddressDaoImpl() {
		super(UserAddress.class);
	}

	

}
