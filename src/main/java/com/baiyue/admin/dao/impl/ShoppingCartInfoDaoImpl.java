package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.ShoppingCartInfoDao;
import com.baiyue.admin.entity.ShoppingCartInfo;
/**
 * 购物车信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class ShoppingCartInfoDaoImpl extends BaseDao<ShoppingCartInfo> implements
		ShoppingCartInfoDao {

	public ShoppingCartInfoDaoImpl() {
		super(ShoppingCartInfo.class);
	}

}
