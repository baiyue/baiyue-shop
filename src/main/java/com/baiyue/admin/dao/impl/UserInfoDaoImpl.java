package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.UserInfoDao;
import com.baiyue.admin.entity.UserInfo;
/**
 * 用户信息数据库处理.
 * @date  2014年3月1日
 */
@Repository
public class UserInfoDaoImpl extends BaseDao<UserInfo> implements UserInfoDao {

	public UserInfoDaoImpl() {
		super(UserInfo.class);
	}

}
