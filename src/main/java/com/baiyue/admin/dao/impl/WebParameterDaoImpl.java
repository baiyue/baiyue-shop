package com.baiyue.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.WebParameterDao;
import com.baiyue.admin.entity.WebParameter;

/**
 * 网站参数数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class WebParameterDaoImpl extends BaseDao<WebParameter> implements WebParameterDao {

	public WebParameterDaoImpl() {
		super(WebParameter.class);
	}

}
