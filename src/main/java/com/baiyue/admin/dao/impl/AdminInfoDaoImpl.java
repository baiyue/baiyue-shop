package com.baiyue.admin.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.AdminInfoDao;
import com.baiyue.admin.entity.AdminInfo;
/**
 * 管理员信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class AdminInfoDaoImpl extends BaseDao<AdminInfo> implements AdminInfoDao{

	public AdminInfoDaoImpl(){
		super(AdminInfo.class);
	}

	@Override
	public AdminInfo getAdminInfoByName(String adminName) {
		String queryString = "from AdminInfo as a where a.name=:name";
		@SuppressWarnings("unchecked")
		List<AdminInfo> result = this.hibernateTemplate.findByNamedParam(queryString, "name", adminName);
		if(result == null || result.size() == 0){
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public List<AdminInfo> getAllAdminInfo() {
		String queryString = "from AdminInfo";
		@SuppressWarnings("unchecked")
		List<AdminInfo> result = this.hibernateTemplate.find(queryString);
		if(result == null || result.size() == 0){
			return null;
		} else {
			return result;
		}
	}

	@Override
	public boolean deleteAdminById(String adminId) {
		if(adminId == null){
			return false;
		}
		String queryString = "from AdminInfo as a where a.adminId=:adminId";
		@SuppressWarnings("unchecked")
		List<AdminInfo> result = this.hibernateTemplate.findByNamedParam(queryString, "adminId", adminId);
		System.out.println("result:"+result);
		if(result == null || result.size() == 0){
			return false;
		} else if(result.size() >1){
			return false;
		}else{
			this.hibernateTemplate.delete(result.get(0));
			return true;
		}
	}

	
}
