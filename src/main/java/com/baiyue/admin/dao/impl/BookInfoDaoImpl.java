package com.baiyue.admin.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.baiyue.admin.common.hibernate.BaseDao;
import com.baiyue.admin.dao.BookInfoDao;
import com.baiyue.admin.entity.AdminInfo;
import com.baiyue.admin.entity.BookInfo;

/**
 * 图书信息数据库操作.
 * @date  2014年3月1日
 */
@Repository
public class BookInfoDaoImpl extends BaseDao<BookInfo> implements BookInfoDao {

	public BookInfoDaoImpl() {
		super(BookInfo.class);
	}
	
	@Override
	public BookInfo getBookInfoByName(String bookName) {
		String queryString = "from BookInfo as b where b.name=:name";
		
		List<BookInfo> result = this.hibernateTemplate.findByNamedParam(queryString, "name", bookName);
		if(result == null || result.size() == 0){
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public List<BookInfo> getAllBookInfo() {
		// TODO Auto-generated method stub
		String queryString = "from BookInfo";
		@SuppressWarnings("unchecked")
		List<BookInfo> result = this.hibernateTemplate.find(queryString);
		if(result == null || result.size() == 0){
			return null;
		} else {
			return result;
		}
	}

	@Override
	public boolean deleteBook(String bookId) {
		if(bookId == null){
			return false;
		}
		String queryString = "from BookInfo as b where b.bookId=:bookId";
		@SuppressWarnings("unchecked")
		List<BookInfo> result = this.hibernateTemplate.findByNamedParam(queryString, "bookId", bookId);
		System.out.println("result:"+result);
		if(result == null || result.size() == 0){
			return false;
		} else if(result.size() >1){
			return false;
		}else{
			this.hibernateTemplate.delete(result.get(0));
			return true;
		}
	}

}
