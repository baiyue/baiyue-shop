package com.baiyue.admin.dao;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.ShoppingCartItem;

public interface ShoppingCartItemDao extends BaseDaoOperation<ShoppingCartItem> {

}
