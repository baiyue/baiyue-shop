package com.baiyue.admin.dao;

import java.util.List;

import com.baiyue.admin.common.hibernate.BaseDaoOperation;
import com.baiyue.admin.entity.AdminInfo;
import com.baiyue.admin.entity.BookInfo;

/**
 * 
 * @date  2014年3月1日
 */
public interface BookInfoDao extends BaseDaoOperation<BookInfo> {
	
	BookInfo getBookInfoByName(String bookName);
	List<BookInfo> getAllBookInfo();
	boolean deleteBook(String bookId);

}
