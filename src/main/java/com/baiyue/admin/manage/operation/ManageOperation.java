package com.baiyue.admin.manage.operation;

public interface ManageOperation<T> {
	T selectById(String id);
}
