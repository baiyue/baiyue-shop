package com.baiyue.admin.manage;

import com.baiyue.admin.entity.OrderItem;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface OrderItemManage extends ManageOperation<OrderItem> {

}
