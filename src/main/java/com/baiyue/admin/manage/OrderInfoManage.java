package com.baiyue.admin.manage;

import com.baiyue.admin.entity.OrderInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface OrderInfoManage extends ManageOperation<OrderInfo> {

}
