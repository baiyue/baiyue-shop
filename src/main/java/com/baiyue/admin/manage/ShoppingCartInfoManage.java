package com.baiyue.admin.manage;

import com.baiyue.admin.entity.ShoppingCartInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface ShoppingCartInfoManage extends ManageOperation<ShoppingCartInfo> {

}
