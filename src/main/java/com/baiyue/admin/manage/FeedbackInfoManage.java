package com.baiyue.admin.manage;

import com.baiyue.admin.entity.FeedbackInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface FeedbackInfoManage extends ManageOperation<FeedbackInfo>{

}
