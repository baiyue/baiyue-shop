package com.baiyue.admin.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.baiyue.admin.dao.AdminInfoDao;
import com.baiyue.admin.entity.AdminInfo;
import com.baiyue.admin.manage.AdminInfoManage;
/**
 * 
 * @date  2014年3月1日
 */
@Service
public class AdminInfoManageImpl implements AdminInfoManage{
	@Autowired
	private AdminInfoDao adminInfoDao;
	
	@Override
	public AdminInfo selectById(String id) {
		return adminInfoDao.selectById(id);
	}

	public AdminInfoDao getAdminInfoDao() {
		return adminInfoDao;
	}

	public void setAdminInfoDao(AdminInfoDao adminInfoDao) {
		this.adminInfoDao = adminInfoDao;
	}


	@Override
	public AdminInfo getAdminInfoByName(String adminName) {
		return adminInfoDao.getAdminInfoByName(adminName);
	}

	@Override
	public List<AdminInfo> getAllAdmin() {
		return adminInfoDao.getAllAdminInfo();
	}

	@Override
	public boolean deleteAdmin(String adminId) {
		return adminInfoDao.deleteAdminById(adminId);
	}

	@Override
	public boolean updateAdmin(AdminInfo adminInfo) {
		return adminInfoDao.update(adminInfo);
	}

	@Override
	public boolean insertAdmin(AdminInfo adminInfo) {
		return adminInfoDao.save(adminInfo);
	}

}
