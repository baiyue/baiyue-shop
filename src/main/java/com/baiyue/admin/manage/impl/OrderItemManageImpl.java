package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.OrderItemDao;
import com.baiyue.admin.entity.OrderItem;
import com.baiyue.admin.manage.OrderItemManage;

@Service
public class OrderItemManageImpl implements OrderItemManage {

	@Autowired
	private OrderItemDao orderItemDao;
	@Override
	public OrderItem selectById(String id) {
		return orderItemDao.selectById(id);
	}
	
	
	
	
	
	
	
	public OrderItemDao getOrderItemDao() {
		return orderItemDao;
	}
	public void setOrderItemDao(OrderItemDao orderItemDao) {
		this.orderItemDao = orderItemDao;
	}

}
