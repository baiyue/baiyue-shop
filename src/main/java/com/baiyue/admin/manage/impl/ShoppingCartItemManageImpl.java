package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.ShoppingCartItemDao;
import com.baiyue.admin.entity.ShoppingCartItem;
import com.baiyue.admin.manage.ShoppingCartItemManage;

@Service
public class ShoppingCartItemManageImpl implements ShoppingCartItemManage {

	@Autowired
	private ShoppingCartItemDao shoppingCartItemDao;
	
	@Override
	public ShoppingCartItem selectById(String id) {
		return shoppingCartItemDao.selectById(id);
	}

	
	
	
	public ShoppingCartItemDao getShoppingCartItemDao() {
		return shoppingCartItemDao;
	}

	public void setShoppingCartItemDao(ShoppingCartItemDao shoppingCartItemDao) {
		this.shoppingCartItemDao = shoppingCartItemDao;
	}

}
