package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.ShoppingCartInfoDao;
import com.baiyue.admin.entity.ShoppingCartInfo;
import com.baiyue.admin.manage.ShoppingCartInfoManage;

@Service
public class ShoppingCartInfoManageImpl implements ShoppingCartInfoManage{

	@Autowired
	private ShoppingCartInfoDao shoppingCartInfoDao;
	@Override
	public ShoppingCartInfo selectById(String id) {
		return shoppingCartInfoDao.selectById(id);
	}
	
	
	
	
	
	public ShoppingCartInfoDao getShoppingCartInfoDao() {
		return shoppingCartInfoDao;
	}
	public void setShoppingCartInfoDao(ShoppingCartInfoDao shoppingCartInfoDao) {
		this.shoppingCartInfoDao = shoppingCartInfoDao;
	}

}
