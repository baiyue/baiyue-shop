package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.UserLoginDao;
import com.baiyue.admin.entity.UserLogin;
import com.baiyue.admin.manage.UserLoginManage;

@Service
public class UserLoginManageImpl implements UserLoginManage {

	@Autowired
	private UserLoginDao userLoginDao;
	@Override
	public UserLogin selectById(String id) {
		return userLoginDao.selectById(id);
	}
	
	
	
	
	public UserLoginDao getUserLoginDao() {
		return userLoginDao;
	}
	public void setUserLoginDao(UserLoginDao userLoginDao) {
		this.userLoginDao = userLoginDao;
	}

}
