package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.OrderInfoDao;
import com.baiyue.admin.entity.OrderInfo;
import com.baiyue.admin.manage.OrderInfoManage;
@Service
public class OrderInfoManageImpl implements OrderInfoManage {

	@Autowired
	private OrderInfoDao orderInfoDao;
	@Override
	public OrderInfo selectById(String id) {
		return orderInfoDao.selectById(id);
	}
	
	
	
	
	
	public OrderInfoDao getOrderInfoDao() {
		return orderInfoDao;
	}
	public void setOrderInfoDao(OrderInfoDao orderInfoDao) {
		this.orderInfoDao = orderInfoDao;
	}

}
