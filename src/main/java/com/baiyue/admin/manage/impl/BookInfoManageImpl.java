package com.baiyue.admin.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.BookInfoDao;
import com.baiyue.admin.entity.BookInfo;
import com.baiyue.admin.manage.BookInfoManage;
/**
 * 
 * @date  2014年3月1日
 */
@Service
public class BookInfoManageImpl implements BookInfoManage {
	@Autowired
	private BookInfoDao bookInfoDao;
	
	@Override
	public BookInfo selectById(String id) {
		return bookInfoDao.selectById(id);
	}

		
	
	public BookInfoDao getBookInfoDao() {
		return bookInfoDao;
	}

	public void setBookInfoDao(BookInfoDao bookInfoDao) {
		this.bookInfoDao = bookInfoDao;
	}
	/*@Override
	public BookInfo getBookInfoByName(String bookName) {
		return bookInfoDao.getBookInfoByName(bookName);
	}*/

	@Override
	public BookInfo getBookInfoByName(String bookName) {
		return bookInfoDao.getBookInfoByName(bookName);
	}
	@Override
	public List<BookInfo> getAllBookInfo() {
		return bookInfoDao.getAllBookInfo();
	}



	@Override
	public boolean deleteBook(String bookId) {
		// TODO Auto-generated method stub
		return bookInfoDao.deleteBook(bookId);
	}



}
