package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.WebParameterDao;
import com.baiyue.admin.entity.WebParameter;
import com.baiyue.admin.manage.WebParameterManage;

@Service
public class WebParameterManageImpl implements WebParameterManage {

	@Autowired
	private WebParameterDao webParameterDao;
	@Override
	public WebParameter selectById(String id) {
		return webParameterDao.selectById(id);
	}
	
	
	
	
	public WebParameterDao getWebParameterDao() {
		return webParameterDao;
	}
	public void setWebParameterDao(WebParameterDao webParameterDao) {
		this.webParameterDao = webParameterDao;
	}

}
