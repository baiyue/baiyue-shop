package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.ReviewInfoDao;
import com.baiyue.admin.entity.ReviewInfo;
import com.baiyue.admin.manage.ReviewInfoManage;

@Service
public class ReviewInfoManageImpl implements ReviewInfoManage {

	@Autowired
	private ReviewInfoDao reviewInfoDao;
	@Override
	public ReviewInfo selectById(String id) {
		return reviewInfoDao.selectById(id);
	}
	
	
	
	
	public ReviewInfoDao getReviewInfoDao() {
		return reviewInfoDao;
	}
	public void setReviewInfoDao(ReviewInfoDao reviewInfoDao) {
		this.reviewInfoDao = reviewInfoDao;
	}

}
