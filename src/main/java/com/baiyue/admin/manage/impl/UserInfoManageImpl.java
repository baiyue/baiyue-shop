package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.UserInfoDao;
import com.baiyue.admin.entity.UserInfo;
import com.baiyue.admin.manage.UserInfoManage;

@Service
public class UserInfoManageImpl implements UserInfoManage {

	@Autowired
	private UserInfoDao userInfoDao;
	@Override
	public UserInfo selectById(String id) {
		return userInfoDao.selectById(id);
	}
	
	
	
	public UserInfoDao getUserInfoDao() {
		return userInfoDao;
	}
	public void setUserInfoDao(UserInfoDao userInfoDao) {
		this.userInfoDao = userInfoDao;
	}

}
