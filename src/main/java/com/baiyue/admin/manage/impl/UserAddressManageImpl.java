package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.UserAddressDao;
import com.baiyue.admin.entity.UserAddress;
import com.baiyue.admin.manage.UserAddressManage;

@Service
public class UserAddressManageImpl implements UserAddressManage {

	@Autowired
	private UserAddressDao userAddressDao;
	@Override
	public UserAddress selectById(String id) {
		return userAddressDao.selectById(id);
	}
	
	
	
	public UserAddressDao getUserAddressDao() {
		return userAddressDao;
	}
	public void setUserAddressDao(UserAddressDao userAddressDao) {
		this.userAddressDao = userAddressDao;
	}

}
