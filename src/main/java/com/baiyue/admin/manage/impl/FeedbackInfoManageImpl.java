package com.baiyue.admin.manage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baiyue.admin.dao.FeedbackInfoDao;
import com.baiyue.admin.entity.FeedbackInfo;
import com.baiyue.admin.manage.FeedbackInfoManage;
/**
 * 
 * @date  2014年3月1日
 */
@Service
public class FeedbackInfoManageImpl implements FeedbackInfoManage {

	@Autowired
	private FeedbackInfoDao feedbackInfoDao;
	
	@Override
	public FeedbackInfo selectById(String id) {
		return feedbackInfoDao.selectById(id); 
	}

	
	
	
	
	public FeedbackInfoDao getFeedbackInfoDao() {
		return feedbackInfoDao;
	}

	public void setFeedbackInfoDao(FeedbackInfoDao feedbackInfoDao) {
		this.feedbackInfoDao = feedbackInfoDao;
	}

}
