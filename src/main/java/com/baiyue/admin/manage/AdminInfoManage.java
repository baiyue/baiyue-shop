package com.baiyue.admin.manage;


import java.util.List;

import com.baiyue.admin.entity.AdminInfo;
import com.baiyue.admin.manage.operation.ManageOperation;
public interface AdminInfoManage extends ManageOperation<AdminInfo>{

	
	AdminInfo getAdminInfoByName(String adminName);
	List<AdminInfo> getAllAdmin();
	boolean deleteAdmin(String adminId);
	boolean updateAdmin(AdminInfo adminInfo);
	boolean insertAdmin(AdminInfo adminInfo);
}
