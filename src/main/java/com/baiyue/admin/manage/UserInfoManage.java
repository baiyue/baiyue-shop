package com.baiyue.admin.manage;

import com.baiyue.admin.entity.UserInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface UserInfoManage extends ManageOperation<UserInfo> {

}
