package com.baiyue.admin.manage;

import java.util.List;

import com.baiyue.admin.entity.BookInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface BookInfoManage extends ManageOperation<BookInfo>{
	BookInfo getBookInfoByName(String bookName);
	List<BookInfo> getAllBookInfo();
	boolean deleteBook(String bookId);

}
