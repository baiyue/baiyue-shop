package com.baiyue.admin.manage;

import com.baiyue.admin.entity.WebParameter;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface WebParameterManage extends ManageOperation<WebParameter> {

}
