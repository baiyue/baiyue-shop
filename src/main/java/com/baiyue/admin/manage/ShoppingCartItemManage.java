package com.baiyue.admin.manage;

import com.baiyue.admin.entity.ShoppingCartItem;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface ShoppingCartItemManage extends ManageOperation<ShoppingCartItem> {

}
