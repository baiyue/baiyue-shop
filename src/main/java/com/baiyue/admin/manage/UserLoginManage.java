package com.baiyue.admin.manage;

import com.baiyue.admin.entity.UserLogin;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface UserLoginManage extends ManageOperation<UserLogin> {

}
