package com.baiyue.admin.manage;

import com.baiyue.admin.entity.ReviewInfo;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface ReviewInfoManage extends ManageOperation<ReviewInfo> {

}
