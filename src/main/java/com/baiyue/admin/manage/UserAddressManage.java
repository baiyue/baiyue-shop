package com.baiyue.admin.manage;

import com.baiyue.admin.entity.UserAddress;
import com.baiyue.admin.manage.operation.ManageOperation;

public interface UserAddressManage extends ManageOperation<UserAddress> {

}
