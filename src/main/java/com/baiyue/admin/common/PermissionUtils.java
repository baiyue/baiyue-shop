package com.baiyue.admin.common;

import javax.servlet.http.HttpServletRequest;

import com.baiyue.admin.entity.AdminInfo;
import com.baiyue.admin.entity.Role;

/**
 * 权限认证.
 * @date  2014年3月18日
 */
public final class PermissionUtils {
	public static MessageResult checkPermission(HttpServletRequest request,String operation,String args) {
		//暂时返回true
		return new MessageResult(true,CheckCode.PermissionYes,"该管理员有权限");
		
		/*AdminInfo adminInfo = (AdminInfo) request.getSession().getAttribute("adminInfo");
		if(adminInfo == null){
			MessageResult result  = new MessageResult();
			result.setSuccess(false);
			result.setMessage("session无效");
			return result;
		}
		if(operation == null){
			MessageResult result  = new MessageResult();
			result.setSuccess(false);
			result.setMessage("操作无权限");
			return result;
		}
		String role = adminInfo.getRole();
		if (role.equals(Role.ROLE_ROOT)) {
			MessageResult result  = new MessageResult();
			result.setSuccess(true);
			return result;
		}
		if (role.equals(Role.ROLE_BOOK_ADMIN)) {
			if (operation.equals(Operation.BOOK_DELETE)
					|| operation.equals(Operation.BOOK_INSERT)
					|| operation.equals(Operation.BOOK_MODIFY)) {
				MessageResult result  = new MessageResult();
				result.setSuccess(true);
				return result;
			} else {
				MessageResult result  = new MessageResult();
				result.setSuccess(false);
				result.setMessage("操作无权限");
				return result;
			}
		}
		if (role.equals(Role.ROLE_ORDER_ADMIN)) {
			if (operation.equals(Operation.ORDER_DELETE)
					|| operation.equals(Operation.ORDER_PROCESS)) {
				MessageResult result  = new MessageResult();
				result.setSuccess(true);
				return result;
			} else {
				MessageResult result  = new MessageResult();
				result.setSuccess(false);
				result.setMessage("操作无权限");
				return result;
			}
		}
		if (role.equals(Role.ROLE_STAFF)){
			if (operation.equals(Operation.REPORT_TABLE)){
				MessageResult result  = new MessageResult();
				result.setSuccess(true);
				return result;
			} else {
				MessageResult result  = new MessageResult();
				result.setSuccess(false);
				result.setMessage("操作无权限");
				return result;
			}
		}
		if (role.equals(Role.ROLE_GUEST)){
			MessageResult result  = new MessageResult();
			result.setSuccess(true);
			return result;
		}
		MessageResult result  = new MessageResult();
		result.setSuccess(false);
		result.setMessage("操作无权限");
		return result;*/
	}
	
	public static MessageResult adminListCheck(HttpServletRequest request){
		AdminInfo adminInfo = (AdminInfo) request.getSession().getAttribute("adminInfo");
		if(adminInfo.getRole().equals(Role.ROLE_GUEST)){
			return new MessageResult(false,CheckCode.PermissionNo,"没有权限获取管理员信息");
		}
		return new MessageResult(true,CheckCode.PermissionYes,"有权限获取管理员信息");
	}
	public static MessageResult adminDeleteCheck(HttpServletRequest request,String adminId){
		AdminInfo adminInfo = (AdminInfo) request.getSession().getAttribute("adminInfo");
		if(adminInfo.getRole().equals(Role.ROLE_ROOT)){
			if(adminInfo.getAdminId().equals(adminId)){
				return new MessageResult(false,CheckCode.PermissionNo,"不能删除自身");
			}else{
				return new MessageResult(true,CheckCode.PermissionYes,"允许删除");
			}
		}
		return new MessageResult(false,CheckCode.PermissionNo,"没有权限");
	}
	
	public static MessageResult adminUpdateCheck(HttpServletRequest request,String adminId){
		AdminInfo adminInfo = (AdminInfo) request.getSession().getAttribute("adminInfo");
		if(adminInfo.getRole().equals(Role.ROLE_ROOT) || adminInfo.getAdminId().equals(adminId)){
			return new MessageResult(true,CheckCode.PermissionYes,"允许修改");
		}
		return new MessageResult(false,CheckCode.PermissionNo,"没有权限");
	}
	
	public static MessageResult adminInsertCheck(HttpServletRequest request,String role){
		AdminInfo adminInfo = (AdminInfo) request.getSession().getAttribute("adminInfo");
		if(adminInfo.getRole().equals(Role.ROLE_ROOT) || adminInfo.getRole().equals(role)){
			return new MessageResult(true,CheckCode.PermissionYes,"允许插入");
		}
		return new MessageResult(false,CheckCode.PermissionNo,"没有权限");
	}
}
