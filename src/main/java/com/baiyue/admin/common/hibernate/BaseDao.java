package com.baiyue.admin.common.hibernate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.baiyue.admin.manage.WebParameterManage;


public class BaseDao<T> implements BaseDaoOperation<T>{
	private static final Logger LOGGER = Logger.getLogger(BaseDao.class);
	@Autowired
	protected HibernateTemplate hibernateTemplate;
	private   Class<?>	clazz;
	
	public BaseDao(Class<?> clazz){
		this.clazz = clazz;
	}
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	@Override
	public boolean save(T entity) {
		if(entity == null){
			return false;
		}
		try{
			this.hibernateTemplate.save(entity);
			return true;
		}catch(Exception e){
			LOGGER.warn(e.getMessage());
			e.printStackTrace();
			return false;
		}finally{
			
		}
	}

	@Override
	public boolean delete(T entity) {
		if(entity == null){
			return false;
		}
		try{
			this.hibernateTemplate.delete(entity);
			return true;
		}catch(Exception e){
			LOGGER.warn(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(T entity) {
		if(entity == null){
			return false;
		}
		try{
			this.hibernateTemplate.update(entity);
			return true;
		}catch(Exception e){
			LOGGER.warn(e.getMessage());
		    e.printStackTrace();
		    return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T selectById(String id) {
		if(id == null) {
			return null;
		} else {
			return (T) hibernateTemplate.get(clazz, id);
		}
	}
	
	
	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}
}
