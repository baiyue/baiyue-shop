package com.baiyue.admin.common.hibernate;

public interface BaseDaoOperation<T> {
	boolean save(T entity);
	boolean delete(T entity);
	boolean update(T entity);
	T selectById(String id);
}
