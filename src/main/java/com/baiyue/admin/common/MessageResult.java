package com.baiyue.admin.common;


import java.util.List;

import com.alibaba.fastjson.JSONObject;

public class MessageResult {
	private boolean success;
	private int     code;
	private String message;
	private Exception exception;
	private List<?>   results;
	
	public List<?> getResults() {
		return results;
	}
	public void setResults(List<?> results) {
		this.results = results;
	}
	public MessageResult(){
		
	}
	public MessageResult(boolean success,String message){
		this.success = success;
		this.message = message;
	}
	public MessageResult(boolean success,int code,String message){
		this.success = success;
		this.code    = code;
		this.message = message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String marsh(){
		return JSONObject.toJSONString(this);
	}
	public static MessageResult unmarsh(String context){
		if(context == null){
			return null;
		}
		return JSONObject.parseObject(context,MessageResult.class);
	}
}
