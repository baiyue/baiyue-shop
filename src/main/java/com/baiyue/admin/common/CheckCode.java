package com.baiyue.admin.common;

public final class CheckCode {
	//sesison无效
	public final static int SessionNo = 100;
	//session有效
	public final static int SessionYes= 101;
	//有权限
	public final static int PermissionYes = 200;
	//没有权限
	public final static int PermissionNo  = 201;
}
